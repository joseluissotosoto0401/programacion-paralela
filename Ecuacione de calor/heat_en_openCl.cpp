#include <iostream>
#include <vector>
#include <CL/cl.hpp>

int main() {
    // Este es un ejemplo simple de cómo resolver una ecuación de calor en 2D usando OpenCL en C++. 
    // En este ejemplo se utiliza una cuadrícula de 1000x1000 puntos, con condiciones iniciales de 25 grados, 
    // y condiciones de frontera de 50 grados en la izquierda, 100 grados en la derecha, 75 grados en la parte 
    // inferior e 0 grados en la parte superior. El código utiliza un paso de tiempo de 0.1 y se ejecuta durante 1000 iteraciones. 
    // El código crea un contexto OpenCL y una cola de comandos, crea un buffer OpenCL a partir del vector de cuadrícula, crea un kernel OpenCL, 
    // establece los argumentos del kernel y ejecuta el kernel durante las iteraciones especificadas. Finalmente, se copia el resultado de nuevo al host 
    // y se imprime la temperatura final del punto central.
    // Hay que tener en cuenta que este es solo un ejemplo básico, y la ecuación de calor real puede ser más compleja y depender de varios factores. También se debe 
    // tener en cuenta que el rendimiento de OpenCL puede variar dependiendo de la plataforma y dispositivo utilizado.
    
    // Create a 2D grid of 1000x1000 points
    int N = 1000;
    std::vector<float> grid(N*N);

    // Set the initial conditions
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            grid[i*N + j] = 25.0; // initial temperature is 25 degrees
        }
    }

    // Set the boundary conditions
    for (int i = 0; i < N; i++) {
        grid[i] = 50.0; // left boundary is 50 degrees
        grid[i*N + N-1] = 100.0; // right boundary is 100 degrees
        grid[(N-1)*N + i] = 75.0; // bottom boundary is 75 degrees
        grid[i*N] = 0.0; // top boundary is 0 degrees
    }

    // Set the time step and number of iterations
    float dt = 0.1;
    int iterations = 1000;

    // Create an OpenCL context and command queue
    cl::Context context = cl::Context::getDefault();
    cl::CommandQueue queue = cl::CommandQueue(context);

    // Create an OpenCL buffer from the grid vector
    cl::Buffer buffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sizeof(float)*N*N, grid.data());

    // Create the OpenCL kernel
    std::string kernelSource = "__kernel void heat_eq(__global float* grid, int N, float dt) { "
                               "    int i = get_global_id(0); "
                               "    int j = get_global_id(1); "
                               "    if (i > 0 && i < N-1 && j > 0 && j < N-1) { "
                               "        grid[i*N + j] += dt * (grid[(i-1)*N + j] + grid[(i+1)*N + j] + grid[i*N + j-1] + grid[i*N + j+1] - 4*grid[i*N + j]) / 4.0; "
                               "    } "
                               "}";
    cl::Program program = cl::Program(context, kernelSource);
    program.build();
    cl::Kernel kernel = cl::Kernel(program, "heat_eq");
    kernel.setArg(0, buffer);
    kernel.setArg(1, N);
    kernel.setArg(2, dt);

    // Execute the kernel for the specified number of iterations
    for (int i = 0; i < iterations; i++) {
        queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(N, N));
    }

    // Copy the result back to the host
    queue.enqueueReadBuffer(buffer, CL, sizeof(float)*N*N, grid.data());

    // Print the final temperature of the center point
    std::cout << "Final temperature of center point: " << grid[(N/2)*N + (N/2)] << " degrees" << std::endl;

    return 0;
}
