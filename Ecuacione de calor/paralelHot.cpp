#include <iostream>
#include <vector>
#include <math.h>
#include <string>
#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <iomanip>
#include "mpi.h"

using namespace std;
int main(int argc, char** argv) {

    double Tmin = 0, Tmax = 0.92, l=1, dx = 0.05, dt=0.001; //valores iniciales
    int i, j, ntau, nx, n;                                  //valores iniciales que deben ser int
    int rank, numtasks;                                   //valores de control mpi
    ntau = (int)(Tmax / dt) + 1;                            //Número de intervalos t tiempo total dividido en la longitud de los dt
    nx = (int)(l / dx) + 1;                                 //Número de x intervalos Longitud total dividio en los tamaños de dt
    n = ntau * nx;                                          //tamaño del vector del espacio
    double* T = (double*)malloc(n * sizeof(double));        //definimos el espacio a trabajar
    double lambda = dt/pow(dx,2);                           //valor lambda

    MPI_Init(&argc,&argv);                                  //iniciamos el mpi
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);                  //iniciamos el idtask como si fuera el rank
    MPI_Comm_size(MPI_COMM_WORLD,&numtasks);                //iniciamos el número de tareas
    // en la pasada entrega se trabajo el espacio como un vector doble, en esta entrega se trabajara como un
    // vector unico, antes se llamaba u ahora T
       
    if (rank == 0) {                                      //definimos el rank 0 para las siguientes operaciones

        printf("MPI_mm ha empezado con %d procesos.\n",numtasks);
        for (i = 0; i < (ntau * nx); i++) {
            T[i] = 0;                                       //vamos a inicializar los valores en 0 para 
        }                                                   //evitar tomar valores de memoria
        //-----------------------------------------------------------------------------------------------
        for (j = 0; j < nx; j++) {
            T[j] = Tmin;                                    //la primera fila la inicializamos en el valor minimo
        }
        //-----------------------------------------------------------------------------------------------
        for (i = 0; i < ntau; i++) {
            T[i * nx + 0] = Tmax;                           //inicializamos la primera columna en el valor maximo
        }
        //-----------------------------------------------------------------------------------------------
        for (i = 0; i < ntau; i++) {
            T[i * nx + (nx - 1)] = Tmax;                    //inicializamos la ultima fila en el vlaor maximo
        }
        //-----------------------------------------------------------------------------------------------
        T[0] = (T[1] + T[nx]) / 2;                         //inicializamos la esquina superior izquierda
        T[nx - 1] = (T[nx - 2] + T[nx + nx - 1]) / 2;      //inicializamos la esquina superior derecha
        //-----------------------------------------------------------------------------------------------
        //mpi send y recive
        MPI_Send(T, n, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD);                      //send mpi
        MPI_Recv(T, n , MPI_DOUBLE, 1, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);  //recive mpi
        //-----------------------------------------------------------------------------------------------
        
        //-----------------------guardamos el vecto T en un archivo txt----------------------------------//
        ofstream heatsolution("heatsolution.txt", std::ofstream::out); //archivo con la solución
        if (heatsolution.is_open()) {
            double xt=0, tx=0;
            int index = 0;
            for (i = 0; i < ntau; i++) {
                tx = (double)dt * i;
                for (j = 0; j < nx; j++) {
                    xt = (double)dx * j;
                    heatsolution << xt << std::setw(10); //primera columna valos de x
                    heatsolution << tx << std::setw(10); //segunda columna tx valores
                    heatsolution << T[index] << "\n";    //valores de T
                    index += 1;
                }
            }
            heatsolution.close();     
        }
        //-------------------------------------------------------------------------------------------------//
        free(T);

    }

    else if (rank == 1) {                                                  //para el rank 1
        MPI_Status status;
        MPI_Probe(0, 0, MPI_COMM_WORLD, &status);
        MPI_Get_count(&status, MPI_DOUBLE, &n);                    
        MPI_Recv(T, n, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); //recive con parametros
        //realizamos la solución de la ecuación de calor! :D esto en el rank 1!
        for (i = 0; i < ntau - 1; i++) {
            for (j = 1; j < nx - 1; j++) {
                T[((i + 1) * nx) + j] = T[(i * nx) + j] + (lambda * ((T[(i * nx) + j + 1] + T[(i * nx) + j - 1] - (2 * T[(i * nx) + j]))));
            }
        }
        MPI_Send(T, n, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);                  //hacemos el send
    }

    MPI_Finalize();
}