#include <iostream>
#include <mpi.h>
#include <time.h>
#include <cstdlib>
#include <string>
#include <vector>

using namespace std;


struct Organismo
{
    short countReproductive = 0;
    short countDead = 0; 
    short xPosition = 0; 
    short yPosition = 0;
    short xMovement = -1; // Indica la posicion donde se presenta un movimiento, si se mantiene en -1 quiere decir que sigue con vida.
    short yMovement = -1; 
    short direction = 2; 
    char kind; 
    // 'P' Indica depredador - 'p' Indica presa - 'n' Espacio vacio
};

const int rows = 100;
const int cols = 100;

bool canMove = false; // Indica si el organismo puede moverse o no

const int numThread = 4;   // Numero de procesos

const int rowsPerProcess = (rows/numThread); // Se asigna a la cantidad de filas que tomara cada proceso.

const int stepsToReproducePrey = 15; // Numero de movimientos antes de que la presa pueda reproducirse
const int stepsToReproducePredator = 25; // Numero de movimientos antes de que el depresador pueda reproducirse.
const int stepsToDie = 28;        // Numero de pasos antes de morir

const int changes = 10000;

int rangeNum; // Identificador del proceso

MPI_Datatype type_organismo; // Tipo de organismo basada en la estructura definida anteriormente

Organismo worldInit[rows][cols]; // Creacion de la matriz inicial

Organismo world[rowsPerProcess][cols]; // Submatriz que procesa cada uno de los procesos 
Organismo worldChange[rowsPerProcess][cols]; // Submatriz de cambio
Organismo ghostMovements[2][cols]; // Lineas de movimiento "fantasma"

void createDataType(MPI_Datatype &datatype);
void fillWorld();
void sendGhostProcess();
void sendNeighborsCells();

void startSimulation();
void changeWorld();

void move(int i, int j);
void predatorMove(int i, int j, vector<pair<int,int>> prey, vector<pair<int,int> > places);
void preyMovementOrReproduction(int i, int j, vector<pair<int,int>> places, char organismo, bool reproduction);
vector<pair<int, int>> nearTo(int i, int j, char organismo);
void deleteOrganism(int i, int j);

void showData();

int main(int arg, char** argvs)
{
    srand(time(NULL));

    MPI_Init(&arg, &argvs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rangeNum);

    createDataType(type_organismo);

    for(int i = 0; i < rowsPerProcess; i++)
        for(int j = 0; j < cols; j++)
        {
            worldChange[i][j].kind = 'n';
        }

    if(rangeNum == 0) 
    {
        fillWorld();
    }

    MPI_Scatter(worldInit, rowsPerProcess*cols, type_organismo, world, rowsPerProcess*cols, type_organismo, 0, MPI_COMM_WORLD); 
    sendGhostProcess();
    sendNeighborsCells();

    int countTurn = 0; // Contador de los cambios realizados

    while(countTurn < changes)
    {
        MPI_Barrier(MPI_COMM_WORLD); 

        if(!canMove) {
            changeWorld(); 
            MPI_Gather(world, rowsPerProcess*cols, type_organismo, worldInit, rowsPerProcess*cols, type_organismo, 0, MPI_COMM_WORLD);
            countTurn++;
            if(rangeNum == 0) {
                showData();
            }
        }
        else {
            sendGhostProcess();
            sendNeighborsCells();
        }
    }

    MPI_Finalize();
}

void createDataType(MPI_Datatype &datatype)
{
    int blockLenghtsOrganismo[8] = {1,1,1,1,1,1,1,1};
    MPI_Aint offsetOrganismo[8];
    offsetOrganismo[0] = offsetof(Organismo, countReproductive); 
    offsetOrganismo[1] = offsetof(Organismo, countDead); 
    offsetOrganismo[2] = offsetof(Organismo, xPosition);
    offsetOrganismo[3] = offsetof(Organismo, yPosition);
    offsetOrganismo[4] = offsetof(Organismo, xMovement);
    offsetOrganismo[5] = offsetof(Organismo, yMovement);
    offsetOrganismo[6] = offsetof(Organismo, direction);
    offsetOrganismo[7] = offsetof(Organismo, kind);
    MPI_Datatype typeOrganismo[8] = {MPI_SHORT, MPI_SHORT, MPI_SHORT, MPI_SHORT, MPI_SHORT, MPI_SHORT, MPI_SHORT, MPI_CHAR};
    MPI_Type_struct(8, blockLenghtsOrganismo, offsetOrganismo, typeOrganismo, &datatype);
    MPI_Type_commit(&datatype);
}

// Funcion para poblar la matriz inicial
void fillWorld() {
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            if(rand()%10 == 0)
            {
               worldInit[i][j].kind = 'p';
            }
            else if(rand()%30 == 0)
            {
                worldInit[i][j].kind = 'P';
            }
            else
            {
                worldInit[i][j].kind = 'n';
            }
        }
    }
}

//Funcion para enviar la primera y ultima fila a los procesos que seran fantasma

void sendGhostProcess() {
    if(numThread == 1)
        return;
    MPI_Request request;
    if(rangeNum == 0) {
        MPI_Isend(world[rowsPerProcess-1], cols, type_organismo, rangeNum+1, 99, MPI_COMM_WORLD, &request);             
    }
    else if(rangeNum == numThread-1) {
        MPI_Isend(world[0], cols, type_organismo, rangeNum-1, 99, MPI_COMM_WORLD, &request);      
    }
    else {
        MPI_Isend(world[0], cols, type_organismo, rangeNum-1, 99, MPI_COMM_WORLD, &request);
        MPI_Isend(world[rowsPerProcess-1], cols, type_organismo, rangeNum+1, 99, MPI_COMM_WORLD, &request);        
    }
}

// Funcion para enviar la primera y ultima fila de los vecinos a los procesos que seran fantasma

void sendNeighborsCells() {
    if(numThread == 1)
        return;
    MPI_Request request;
    if(rangeNum == 0) {
        MPI_Irecv(ghostCells[1], cols, type_organismo, rangeNum+1, 99, MPI_COMM_WORLD, &request);
    }
    else if(rangeNum == numThread-1) {
        MPI_Irecv(ghostCells[0], cols, type_organismo, rangeNum-1, 99, MPI_COMM_WORLD, &request);
    }
    else {
        MPI_Irecv(ghostCells[0], cols, type_organismo, rangeNum-1, 99, MPI_COMM_WORLD, &request);
        MPI_Irecv(ghostCells[1], cols, type_organismo, rangeNum+1, 99, MPI_COMM_WORLD, &request);
    }
}

// Funcion para iniciar el proceso de simulacion
void startSimulation() {
    if(canMove) {
        canMove = false;
        for(int k = 0; k < 2; k++) {
            for(int i = 0; i < cols; i++) {

                if((ghostCells[k][i].direction == 1 && k == 0 && rangeNum != 0) 
                || (ghostCells[k][i].direction == 0 && k == 1 && rangeNum != numThread-1)) {

                    if(ghostCells[k][i].countDead > stepsToDie) {
                        continue;
                    }
                    int xPosition = ghostCells[k][i].xPosition;
                    int yPosition = ghostCells[k][i].yPosition;
                    int xMovement = ghostCells[k][i].xMovement;
                    int yMovement = ghostCells[k][i].yMovement;

                    if(xMovement != -1 && yMovement != -1) {   
                        worldChange[xMovement][yMovement] = ghostCells[k][i];
                        worldChange[xMovement][yMovement].countReproductive = 0;
                    }
                    else {
                        if(world[xPosition][yPosition].kind == 'p' && ghostCells[k][i].kind == 'P') {
                            deleteOrganism(xPosition,yPosition);
                            ghostCells[k][i].countDead = 0; 
                        }
                        worldChange[xPosition][yPosition] = ghostCells[k][i]; 
                    }
                }
            }
        }

        for(int i = 0; i < rowsPerProcess; i++) {   
            for(int j = 0; j < cols; j++) {

                if(world[i][j].kind == 'P' &&
                  (world[i][j].direction == 2 || (world[i][j].direction != 2 && world[i][j].xMovement != -1))) {
                    
                    if(world[i][j].countDead > stepsToDie){
                        deleteOrganism(i, j);
                        continue;
                    }
                    int xPosition = world[i][j].xPosition;
                    int yPosition = world[i][j].yPosition;
                    int xMovement = world[i][j].xMovement;
                    int yMovement = world[i][j].yMovement;
                    if(xMovement != -1 && yMovement != -1) {

                        if(worldChange[xMovement][yMovement].kind == 'n') {
                            worldChange[xMovement][yMovement] = world[i][j];
                            worldChange[xMovement][yMovement].countReproductive = 0;
                        }
                        if(worldChange[xPosition][yPosition].kind == 'n') {
                            worldChange[xPosition][yPosition] = world[i][j];
                            worldChange[xPosition][yPosition].countReproductive = 0;
                        }
                    }
                    else {
                        if(worldChange[xPosition][yPosition].kind == 'n') {
                            if(world[xPosition][yPosition].kind == 'p') {
                                deleteOrganism(xPosition,yPosition);
                                world[i][j].countDead = 0;
                            }
                            worldChange[xPosition][yPosition] = world[i][j];
                        }   
                    }
                }
            }
        }

        for(int i = 0; i < rowsPerProcess; i++) {   
            for(int j = 0; j < cols; j++) {

                if(world[i][j].kind == 'p' &&
                  (world[i][j].direction == 2 || (world[i][j].direction != 2 && world[i][j].xMovement != -1))) {
                    int xPosition = world[i][j].xPosition;
                    int yPosition = world[i][j].yPosition;
                    int xMovement = world[i][j].xMovement;
                    int yMovement = world[i][j].yMovement;
                    if(xMovement != -1 && yMovement != -1) {
                        if(worldChange[xMovement][yMovement].kind == 'n') {
                            worldChange[xMovement][yMovement] = world[i][j];
                            worldChange[xMovement][yMovement].countReproductive = 0;
                        }
                        if(worldChange[xPosition][yPosition].kind == 'n') {
                            worldChange[xPosition][yPosition] = world[i][j];
                            worldChange[xPosition][yPosition].countReproductive = 0;
                        }
                    }
                    else if(worldChange[xPosition][yPosition].kind == 'n')
                        worldChange[xPosition][yPosition] = world[i][j];
                }
            }
        }
    }
    else {
        for(int i = 0; i < rowsPerProcess; i++) {
            for(int j = 0; j < cols; j++) {
                worldChange[i][j].kind = 'n';
                worldChange[i][j].countDead = 0;
                worldChange[i][j].countReproductive = 0;
            }
        }
            
        for(int i = 0; i < rowsPerProcess; i++) {   
            for(int j = 0; j < cols; j++) {
                if(world[i][j].kind == 'P') {
                    move(i , j);   
                }
            }
        }

        for(int i = 0; i < rowsPerProcess; i++) {
            for(int j = 0; j < cols; j++) {
                if(world[i][j].kind == 'p') {
                    move(i , j);
                }
            }
        }

        canMove = true;
        for(int i = 0; i < rowsPerProcess; i++) {
            for(int j = 0; j < cols; j++) {
                worldChange[i][j].kind = 'n';
                worldChange[i][j].countDead = 0;
                worldChange[i][j].countReproductive = 0;
            }
        }
    }
}

// Funcion para cambiar la submatriz auxiliar por la matriz principal

void changeWorld() {
    for(int i = 0; i < rowsPerProcess; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            // Obtencion de un organismo temporal
            Organismo temp = worldChange[i][j];
            worldChange[i][j] = world[i][j];
            world[i][j] = temp;
        }
    }
}

// Funcion que toma la decision de la proxima posicion que tendra un organismo

void move(int i, int j) {

    if(world[i][j].kind == 'P') {
        world[i][j].countDead++;
    }
    world[i][j].countReproductive++;

    vector<pair<int, int>> prey; 
    vector<pair<int, int>> places; 

    if((world[i][j].kind == 'P' && world[i][j].countReproductive > stepsToReproducePredator) ||
      (world[i][j].kind == 'p' && world[i][j].countReproductive > stepsToReproducePrey)) {
            places = nearTo(i, j, 'n');
            preyMovementOrReproduction(i, j, places, world[i][j].kind, true);
            return;
        }

    world[i][j].xMovement = -1;
    world[i][j].yMovement = -1;

    if(world[i][j].kind == 'P') {
        prey = nearTo(i, j, 'p'); 
    }
    places = nearTo(i, j, 'n');

    if(world[i][j].kind == 'P') {
        predatorMove(i,j,prey,places);
    } else if(world[i][j].kind == 'p') {
        preyMovementOrReproduction(i,j,places, 'p', false);
    }
}

// Funcion para saber que tipo de organismo se encuentra en la celda subyacente
vector<pair<int, int>> nearTo(int i, int j, char organismo) {

    vector<pair<int, int> > nearer;
    if(i+1 < rowsPerProcess && world[i+1][j].kind == organismo)
        nearer.push_back({i+1,j});
    if(j+1 < cols && world[i][j+1].kind == organismo)
        nearer.push_back({i,j+1});
    if(i+1 < rowsPerProcess && j+1 < cols && world[i+1][j+1].kind == organismo)
        nearer.push_back({i+1,j+1});
    if(i-1 >= 0 && world[i-1][j].kind == organismo)
        nearer.push_back({i-1,j});
    if(j-1 >= 0 && world[i][j-1].kind == organismo)
        nearer.push_back({i,j-1});
    if(i-1 >= 0 && j-1 >= 0 && world[i-1][j-1].kind == organismo) 
        nearer.push_back({i-1,j-1});
    if(i-1 >= 0 && j+1 < cols && world[i-1][j+1].kind == organismo)
        nearer.push_back({i-1,j+1});
    if(i+1 < rowsPerProcess && j-1 >= 0  && world[i+1][j-1].kind == organismo)
        nearer.push_back({i+1,j-1});

    if(numThread > 1) {

        if(i+1 >= rowsPerProcess && rangeNum != numThread-1 && ghostCells[1][j].kind == organismo)
        nearer.push_back({i+1,j});
    
        if(i-1 < 0 && rangeNum != 0 && ghostCells[0][j].kind == organismo)
            nearer.push_back({i-1,j});
        
        if(i-1 < 0 && j-1 >= 0 && rangeNum != 0 && ghostCells[0][j-1].kind == organismo) 
            nearer.push_back({i-1,j-1});

        if(i-1 < 0 && j+1 < cols && rangeNum != 0 && ghostCells[0][j+1].kind == organismo)
            nearer.push_back({i-1,j+1});
        
        if(i+1 >= rowsPerProcess && j-1 >= 0 && rangeNum != numThread-1 && ghostCells[1][j-1].kind == organismo)
            nearer.push_back({i+1,j-1});
        
        if(i+1 >= rowsPerProcess && j+1 < cols && rangeNum != numThread-1 && ghostCells[1][j+1].kind == organismo)
            nearer.push_back({i+1,j+1});
    }

    return nearer;

}

// Funcion para decidir el movimiento futuro de un depredador

void predatorMove(int i, int j, vector<pair<int,int>> prey, vector<pair<int,int> > places) {
    int x = -1;
    int y = -1;
    int cont = 0;
    bool location = false; 
    bool ghost0 = false; 
    bool ghost1 = false;
    bool eatPrey = false; 

    if(prey.size() > 0) {
        while(true) {
            if(cont >= prey.size()*5) {
                break;
            }
            int pos = rand() % prey.size(); 
            x = prey[pos].first;
            y = prey[pos].second;
            cont++;
            if(x >= rowsPerProcess && rangeNum != numThread-1) {
                if(ghostCells[1][y].kind == 'n') {
                    ghost1 = true;
                    location = true;
                    eatPrey = true;
                    break;
                }
            } else if(x < 0 && rangeNum != 0) {
                if(ghostCells[0][y].kind == 'n') {
                    ghost0 = true;
                    location = true;
                    eatPrey = true;
                    break;
                }
            } else if(worldChange[x][y].kind == 'n') {
                ghost0 = false;
                ghost1 = false;
                location = true;
                eatPrey = true;
                break;
            }
        }
    }
    else if(places.size() > 0) {
        while(true) {

            if(cont >= places.size()*5) {
                break;
            }
            int pos = rand() % places.size();
            x = places[pos].first;
            y = places[pos].second;
            cont++;
            if(x >= rowsPerProcess && rangeNum != numThread-1) {
                if(ghostCells[1][y].kind == 'n') {
                    ghost1 = true;
                    location = true;
                    x = 1;
                    break;
                }
            } else if(x < 0 && rangeNum != 0) {
                if(ghostCells[0][y].kind == 'n') {
                    ghost0 = true;
                    location = true;
                    x = 0;
                    break;
                }
            } else if(worldChange[x][y].kind == 'n') {
                ghost0 = false;
                ghost1 = false;
                location = true;
                break;
            }
        }
    }

    if(location) {
        world[i][j].xPosition = x;
        world[i][j].yPosition = y;
        world[i][j].direction = 2;

        if(ghost0) {
            ghostCells[0][y] = world[i][j];
            world[i][j].direction = 0;
            world[i][j].xPosition = rowsPerProcess-1;
        } else if(ghost1) {
            ghostCells[1][y] = world[i][j];
            world[i][j].direction = 1;
            world[i][j].xPosition = 0;
        } else {
            worldChange[x][y] = world[i][j];
        }
    } else {
        worldChange[i][i] = world[i][j];
        world[i][j].xPosition = i;
        world[i][j].yPosition = j;
        world[i][j].direction = 2;
    }
}

// Funcion para decidir el movimiento de una presa o su reproduccion

void preyMovementOrReproduction(int i, int j, vector<pair<int,int>> places, char organismo, bool reproduction) {
    int x = -1;
    int y = -1;
    int cont = 0;
    bool location = false;
    bool ghost0 = false;
    bool ghost1 = false;
    if(places.size() > 0) {
        while(true) {
            if(cont >= places.size()*5) {
                break;
            }
            int pos = rand() % places.size();
            x = places[pos].first;
            y = places[pos].second;
            cont++;
            if(x >= rowsPerProcess && rangeNum != numThread-1) {
                if(ghostCells[1][y].kind == 'n') {
                    ghost1 = true;
                    location = true;
                    break;
                }
            } else if(x < 0 && rangeNum != 0) {
                if(ghostCells[0][y].kind == 'n') {
                    ghost0 = true;
                    location = true;
                    break;
                }
            } else if(worldChange[x][y].kind == 'n') {
                ghost0 = false;
                ghost1 = false;
                location = true;
                break;
            }
        }
    }

    if(location) {
        if(reproduction) {
            worldChange[i][i] = world[i][j];
            world[i][j].xPosition = i;
            world[i][j].yPosition = j;
            world[i][j].yMovement = y;
            world[i][j].xMovement = x;
            world[i][j].direction = 2;

            if(ghost0) {
                ghostCells[0][y] = world[i][j];
                world[i][j].direction = 0;
                world[i][j].xMovement = rowsPerProcess-1;
            } else if(ghost1) {
                ghostCells[1][y] = world[i][j];
                world[i][j].direction = 1;
                world[i][j].xMovement = 0;
            } else {
                worldChange[x][y] = world[i][j];
            }
        } else {
            world[i][j].xPosition = x;
            world[i][j].yPosition = y;
            world[i][j].direction = 2;
            
            if(ghost0) {
                ghostCells[0][y] = world[i][j];
                world[i][j].direction = 0;
                world[i][j].xPosition = rowsPerProcess-1;
            }
            else if(ghost1) {
                ghostCells[1][y] = world[i][j];
                world[i][j].direction = 1;
                world[i][j].xPosition = 0;
            } else
                worldChange[x][y] = world[i][j];
        } 
    } else {
        worldChange[i][i] = world[i][j];
        world[i][j].xPosition = i;
        world[i][j].yPosition = j;
        world[i][j].direction = 2;
    }
}

// Funcion para eliminar un organismo una vez que muera

void deleteOrganism(int i, int j) {
    world[i][j].kind='n';
    world[i][j].countDead = 0;
    world[i][j].countReproductive = 0;
}

// Funcion para imprimir los datos en la terminal

void showData() {
    int size = 4;
    int prey = 0;
    int predator = 0;
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < cols; j++)
        {
            if(worldInit[i][j].kind == 'p') {
                prey++;
            }
            else if(worldInit[i][j].kind == 'P') {   
                predator++;
            }
        }
    }
    cout<<"prey: "<<prey<<endl<<"Predator: "<<predator<<endl<<endl;
}