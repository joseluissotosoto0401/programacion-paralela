#include <iostream>
#include <cmath>
#include <cstdlib>
#include <mpi.h>

#define N 1000
#define dt 0.01
#define G 6.67430e-11

struct Particle
{
    double x, y, z, vx, vy, vz, m;
};

double dist(Particle p1, Particle p2)
{
    double dx = p1.x - p2.x;
    double dy = p1.y - p2.y;
    double dz = p1.z - p2.z;
    return std::sqrt(dx*dx + dy*dy + dz*dz);
}

double calc_force(Particle p1, Particle p2)
{
    double d = dist(p1, p2);
    return G * p1.m * p2.m / (d * d);
}

int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);

    int world_size, rank;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    Particle particles[N];

    for (int i = 0; i < N; i++)
    {
        particles[i].x = rand() % 100;
        particles[i].y = rand() % 100;
        particles[i].z = rand() % 100;
        particles[i].vx = 0;
        particles[i].vy = 0;
        particles[i].vz = 0;
        particles[i].m = rand() % 100 + 1;
    }

    for (double t = 0; t < 10; t += dt)
    {
        for (int i = rank; i < N; i += world_size)
        {
            for (int j = 0; j < N; j++)
            {
                if (i != j)
                {
                    double force = calc_force(particles[i], particles[j]);
                    particles[i].vx += force * (particles[j].x - particles[i].x) / dist(particles[i], particles[j]) * dt / particles[i].m;
                    particles[i].vy += force * (particles[j].y - particles[i].y) / dist(particles[i], particles[j]) * dt / particles[i].m;
                    particles[i].vz += force * (particles[j].z - particles[i].z) / dist(particles[i], particles[j]) * dt / particles[i].m;
                }
            }
        }

        MPI_Allgather(MPI_IN_PLACE, 0, MPI_DATATYPE_NULL, particles, sizeof(Particle), MPI_BYTE, MPI_COMM_WORLD);

        for (int i = 0; i < N; i++){
            particles[i].x += particles[i].vx * dt;
            particles[i].y += particles[i].vy * dt;
            particles[i].z += particles[i].vz * dt;
        }
        MPI_Finalize();
        return 0;
    }
}